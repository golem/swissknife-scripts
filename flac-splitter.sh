#!/bin/bash
# Splits a FLAC file using relative CUE

if [ $# -ne 1 ]; then
	kdialog --error "Non si è specificato alcun file FLAC"
	exit 1
fi

flacfile=$1

if [[ ! -f $flacfile ]]; then
	kdialog --error "Non si è specificato alcun file FLAC"
	exit 1
fi

flacpath=xpath=${flacfile%/*} 

cuefile=$(kdialog --title "Seleziona un file .cue" --getopenfilename "$flacpath" 'application/x-cue')

if [ $? -ne 0 ]; then
	kdialog --error "Non si è specificato alcun file CUE"
	exit 1
elif [[ ! -f $flacfile ]]; then
	kdialog --error "Non si è specificato alcun file CUE"
	exit 1
fi

log=$(shnsplit -f "$cuefile" -t %n-%t -o flac "$flacfile")

if [ $? -ne 0 ]; then
	kdialog --error "Splitting terminato con errori\n"$log
	exit 1
else
	kdialog --msgbox "Splitting terminato con successo"
fi
