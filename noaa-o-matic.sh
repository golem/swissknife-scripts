#!/bin/bash
# Converts NOAA APT audio sample in corresponding image
# Requires wxtoimg

if [[ $# -ne 1 ]]; then
    echo "Usage:"
    echo "$0 filename.wav"
    exit 1
fi

tmpdir=/tmp

# strip path
filename=$(basename -- "$1")
filename="${filename%.*}"

sox $1 -r 11025 $tmpdir/$filename-resampled.wav channels 1 
wxtoimg -t n -o -e HVC $tmpdir/$filename-resampled.wav $filename.png
rm $tmpdir/$filename-resampled.wav
