#!/usr/bin/python3

# Rename all files in a folder, using new names from a txt file
# by lucam

from os import listdir, getcwd, rename
from os.path import isfile, join

# Path and files
FILES_PATH = getcwd()
NEW_NAMES_TXT = FILES_PATH + "/new_names.txt"


print("[I]: working on", FILES_PATH)

# read old names from folder
old_files = [f for f in listdir(FILES_PATH) if (isfile(join(FILES_PATH, f)) and f!="new_names.txt")]
old_files.sort()

# read new names from file
with open(NEW_NAMES_TXT, "r") as names_file:
	new_files = [line[:-1] for line in names_file]

print(old_files)

# check sizes
len_old_files = len(old_files)
len_new_files = len(new_files)
print(len_old_files)
print(len_new_files)
if len_old_files != len_new_files:
	print("[E]: files and names are different size!")
	exit(1)

# rename
for i in range(len_old_files):
	print("Renaming", old_files[i], "to" , new_files[i])
	rename(old_files[i], new_files[i])


# si poteva fare in bash? sì
# avevo voglia? no
