# Confronta il contenuto di due file binari usando vimdiff

# È necessario fornire i due file come argomenti
if [ $# -ne 2 ]; then
	echo "$0 file1 file2"
	exit -1
fi

# Verifica esistenza file
if [ ! -f "$1" ]; then
	echo "File $1 not found"
	exit -1
fi

if [ ! -f "$2" ]; then
	echo "File $2 not found"
	exit -1
fi

vimdiff <(xxd "$1") <(xxd "$2")
