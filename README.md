# swissknife-scripts

Archivio di script utili per l'uso quotidiano di Linux

Scripts archive useful for everyday Linux use

## Script contenuti
- noaa-o-matic: decodifica una registrazione wav del satellite NOAA in un'immagine meteo
- flac-splitter: divide un file FLAC in più tracce usando un CUE
- bindiff: confronta il contenuto binario di due file. Richiede vimdiff e xxd.

## Appunti per script futuri
- Re-impaginazione di PDF
  - Selezione pagine, compressione "due in un foglio"

  ``pdfjam --nup 1x2 in.pdf pages --outfile out.pdf``
