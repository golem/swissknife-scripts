#### Feature custom per lo zshrc

### Alias utili
## Sostituisce cat con bat (batcat su Ubuntu)
alias cat='bat --paging=never'
## Sostituisce le shortcut di ls con exa
alias la='exa -abghHliS'
alias ll='exa -bghHliS'


## Attiva un filtro più "raffinato" per la ricerca nella history
# https://grml.ml.grml.narkive.com/pGRsY1uU/zsh-history-completion-via-arrow-keys
autoload -Uz up-line-or-beginning-search
zle -N up-line-or-beginning-search
autoload -Uz down-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey '\eOA' up-line-or-beginning-search
bindkey '\e[A' up-line-or-beginning-search
bindkey '\eOB' down-line-or-beginning-search
bindkey '\e[B' down-line-or-beginning-search
